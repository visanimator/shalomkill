const puppeteer = require('puppeteer');
const cred = require('./cred.json');

(async () => {
    const browser = await puppeteer.launch({
        headless: false,
        userDataDir: "./user_data",
        executablePath: 'C:/Program Files/Google/Chrome/Application/chrome.exe',
        defaultViewport: null,
        args: ['--start-maximized'],
    });
    const page = await browser.newPage();
    await page.goto('https://www.twitch.tv');
    const element = await page.$x('//*[@data-a-target="login-button"]');

    if (element.length > 0) {
        await login(page);
    }

    await page.goto('https://www.twitch.tv/shalomraj', {
        waitUntil: 'load',
        timeout: 0
    });
    await looper(page);



})();

async function looper(page) {
    if (await isLive(page)) {
        console.log('is live');
        try {
            await doIt(page);
        } catch (e) {
            console.log(e);
        }

        setTimeout(async () => {
            await looper(page);
        }, 60000);
    } else {
        console.log('is !live');
        await page.browser.close();
    }

}

async function login(page) {
    let loginButton = await page.$x('//*[@data-a-target="login-button"]');
    loginButton[0].click();
    await page.waitForXPath('//*[@id="login-username"]');
    await page.type('#login-username', cred.username, { delay: 100 });
    await page.type('#password-input', cred.password, { delay: 200 });
    let loginButtons = await page.$x('//*[@data-a-target="passport-login-button"]');
    loginButtons[0].click();

}

async function isLive(page) {
    let liveStatus = await page.$x('//div[contains(@class, "live-indicator-container")]');
    return (liveStatus.length > 0);
}




async function doIt(page) {
    await page.waitForXPath('//button[@aria-label="Points Balance"]');
    let pointsButton = await page.$x('//button[@aria-label="Points Balance"]');
    pointsButton[0].click();
    await page.waitForXPath('//*[@id="channel-points-reward-center-body"]/div/div/div[4]/div/button');

    let hydrateButton = await page.$x('//*[@id="channel-points-reward-center-body"]/div/div/div[4]/div/button');
    hydrateButton[0].click();
    await page.waitForXPath('//*[@id="channel-points-reward-center-body"]/div/div/div[3]/button');
    let hydrateConfirm = await page.$x('//*[@id="channel-points-reward-center-body"]/div/div/div[3]/button');
    hydrateConfirm[0].click();

}